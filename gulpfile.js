var gulp           = require('gulp'),
    less           = require('gulp-less'),
    autoprefixer   = require('gulp-autoprefixer'),
    notify         = require('gulp-notify'),
    concat         = require('gulp-concat'),
    browserSync    = require('browser-sync');


gulp.task('less', function(){
    return gulp.src(['app/less/**/*.less', '!app/less/**/_*.less'])
        .pipe(less())
        .on('error', notify.onError(function (error) {
            return "A task less error occurred: " + error.message;
        }))
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.reload({stream: true})) 
});

// gulp.task('scripts', function(){
// 	return gulp.src([
//         // 'app/libs/**/*.js',
//         // 'node_modules/vue/dist/vue.js',
//         'app/libs/jquery-3.3.1.min.js',
//         'app/libs/slick/slick.min.js'
//         ])
//         .pipe(concat('libs.js')
//         .on('error', notify.onError(function (error) {
//             return "A task scripts error occurred: " + error.message;
//         })))
// 		.pipe(gulp.dest('app/js'))
//         .pipe(browserSync.reload({stream: true}));
// });

// gulp.task('style', function(){
// 	return gulp.src([
//         // 'app/libs/**/*.css',
//         'app/libs/slick/slick.css',
//         'app/libs/slick/slick-theme.css',
//         'app/libs/animate.css'
//         ])
//         .pipe(concat('libs.css')
//         .on('error', notify.onError(function (error) {
//             return "A task style error occurred: " + error.message;
//         })))
// 		.pipe(gulp.dest('app/css'))
//         .pipe(browserSync.reload({stream: true}));
// });

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false,
        // open: false,
        // online: false, // Work Offline Without Internet Connection
        // tunnel: true, tunnel: "projectname", // Demonstration page: http://projectname.localtunnel.me
    })
});

gulp.task('watch', ['browser-sync', 'less',], function() {
	gulp.watch('app/less/**/*.less', ['less']);
    //gulp.watch('app/js/**/*.js', ['my-scripts']);
	//gulp.watch('/gulpfile.js', ['style', 'scripts']);
    //gulp.watch('app/**/*.html', ['html']);
	//gulp.watch('app/js/**/*.js', browserSync.reload);
	gulp.watch('app/css/**/*.css', browserSync.reload);
	gulp.watch('app/**/*.html', browserSync.reload);
});

gulp.task('default', ['watch']);